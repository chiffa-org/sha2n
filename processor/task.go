package processor

import (
	"crypto"
	_ "crypto/sha256"
	_ "crypto/sha512"
	"fmt"
	"hash"

	"github.com/gofrs/uuid"
)

type Task struct {
	id     uuid.UUID
	hash   hash.Hash
	rounds int
	sum    []byte
}

type Result struct {
	ID  uuid.UUID
	Sum []byte
}

// NewTask creates Task to be passed to Processor making single hash round in advance.
func NewTask(id uuid.UUID, payload string, h crypto.Hash, rounds int) (*Task, error) {
	if !h.Available() {
		return nil, fmt.Errorf("hash function %v unavailable", h)
	}
	if rounds < 1 {
		return nil, fmt.Errorf("non-positive rounds count")
	}

	f := h.New()

	// apply hash once right now to free payload
	_, _ = f.Write([]byte(payload))
	sum := f.Sum(nil)
	f.Reset()

	task := &Task{
		id:     id,
		hash:   f,
		rounds: rounds - 1,
		sum:    sum,
	}
	return task, nil
}

// Next makes up to n rounds of hashing returning actual applied rounds count.
// n <= 0 means no limits
func (t *Task) Next(n int) int {
	if t.rounds < n || n <= 0 {
		n = t.rounds
	}
	t.rounds -= n

	for i := 0; i < n; i++ {
		_, _ = t.hash.Write(t.sum)
		t.hash.Sum(t.sum[:0])
		t.hash.Reset()
	}

	return n
}

// IsCompleted reports if all rounds were calculated.
func (t *Task) IsCompleted() bool {
	return t.rounds == 0
}

// Result returns task hashing result. It must be called on completed tasks only.
// Calling Next after Result on uncompleted task will result to panic.
func (t *Task) Result() Result {
	t.hash = nil

	return Result{
		ID:  t.id,
		Sum: t.sum,
	}
}

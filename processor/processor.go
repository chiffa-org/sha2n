package processor

import (
	"context"
	"crypto"
	"fmt"
	"sync"

	"github.com/gofrs/uuid"
	"go.uber.org/zap"

	"bitbucket.org/chiffa-org/sha2n/store"
)

type Processor struct {
	ts  *store.Tasks
	log *zap.Logger

	roundsAtOnce int

	in    chan *Task
	close chan struct{}

	wg sync.WaitGroup
}

func New(ts *store.Tasks, log *zap.Logger, hashWorkers, queueSize, roundsAtOnce int) (*Processor, error) {
	if hashWorkers <= 0 {
		return nil, fmt.Errorf("non-positive hash workers count")
	}
	if queueSize <= 0 {
		return nil, fmt.Errorf("non-positive queue size")
	}

	p := &Processor{
		ts:  ts,
		log: log,

		roundsAtOnce: roundsAtOnce,

		in:    make(chan *Task, queueSize),
		close: make(chan struct{}),
	}

	p.wg.Add(hashWorkers)
	for i := 0; i < hashWorkers; i++ {
		go p.hashWorker()
	}

	return p, nil
}

func (p *Processor) hashWorker() {
	defer p.wg.Done()

	for {
		select {
		case <-p.close:
			return
		case task := <-p.in:
		l:
			for {
				// make part of hash rounds
				task.Next(p.roundsAtOnce)

				// write result if task is completed
				if task.IsCompleted() {
					result := task.Result()

					err := p.ts.SetTaskResult(result.ID, result.Sum)
					if err == nil {
						break l
					}

					p.log.Error("write result to store", zap.Error(err), zap.Any("task id", result.ID))
				}

				// trying to send task to queue end
				// to allow small tasks in queue to be processed
				select {
				case <-p.close:
					return
				case p.in <- task:
					break l
				default:
				}
			}
		}
	}
}

func (p *Processor) AddTask(id uuid.UUID, payload string, h crypto.Hash, rounds int) error {
	return p.AddTaskContext(context.Background(), id, payload, h, rounds)
}

func (p *Processor) AddTaskContext(ctx context.Context, id uuid.UUID, payload string, h crypto.Hash, rounds int) error {
	task, err := NewTask(id, payload, h, rounds)
	if err != nil {
		return err
	}

	select {
	case p.in <- task:
		return nil
	case <-ctx.Done():
		return fmt.Errorf("processor is too busy")
	}
}

func (p *Processor) Close() error {
	close(p.close)
	p.wg.Wait()

	return nil
}

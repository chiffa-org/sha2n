package api

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/gofrs/uuid"
	"go.uber.org/zap"

	"bitbucket.org/chiffa-org/sha2n/store"
)

func (api *API) GetTask(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.FromString(chi.URLParam(r, "task-id"))
	if err != nil {
		_ = render.Render(w, r, NewFailResponse(http.StatusBadRequest, "Malformed UUID"))
		return
	}

	dbTask, err := api.ts.GetTaskContext(r.Context(), id)
	if err != nil {
		api.log.Error("get task", zap.Any("task id", id), zap.Error(err))

		_ = render.Render(w, r, NewFailResponse(http.StatusInternalServerError, ""))
		return
	}

	if dbTask == nil {
		_ = render.Render(w, r, NewFailResponse(http.StatusNotFound, ""))
		return
	}

	_ = render.Render(w, r, NewTaskResponse(http.StatusOK, dbTask))
}

func (api *API) PostTask(w http.ResponseWriter, r *http.Request) {
	var taskReq TaskRequest

	err := render.Bind(r, &taskReq)
	if err != nil {
		_ = render.Render(w, r, NewFailResponse(http.StatusBadRequest, err.Error()))
		return
	}

	hashName := store.ParseHashName(taskReq.HashFunc)
	id, err := api.ts.AddTaskContext(r.Context(), taskReq.Payload, hashName, taskReq.HashRounds)
	if err != nil {
		api.log.Error("add task to store", zap.Error(err))

		_ = render.Render(w, r, NewFailResponse(http.StatusInternalServerError, ""))
		return
	}

	err = api.proc.AddTaskContext(r.Context(), id, taskReq.Payload, store.HashByName(hashName), taskReq.HashRounds)
	if err != nil {
		api.log.Error("add task to processor", zap.Any("task id", id), zap.Error(err))

		_ = render.Render(w, r, NewFailResponse(http.StatusInternalServerError, ""))

		// remove from store to avoid redundant processing in future
		err := api.ts.DeleteTask(id)
		if err != nil {
			api.log.Error("delete task from store", zap.Any("task id", id), zap.Error(err))
		}

		return
	}

	_ = render.Render(w, r, NewTaskIDResponse(http.StatusOK, id))
}

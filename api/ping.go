package api

import (
	"net/http"

	"github.com/go-chi/render"
	"go.uber.org/zap"
)

func (api *API) Ping(w http.ResponseWriter, r *http.Request) {
	err := api.ts.PingContext(r.Context())
	if err != nil {
		api.log.Error("ping tasks store", zap.Error(err))

		_ = render.Render(w, r, NewFailResponse(http.StatusInternalServerError, "Tasks store unavailable"))
		return
	}

	_ = render.Render(w, r, NewOkResponse(http.StatusOK, "Pong"))
}

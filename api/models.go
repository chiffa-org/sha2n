package api

import (
	"encoding/hex"
	"fmt"
	"net/http"

	"github.com/go-chi/render"
	"github.com/gofrs/uuid"

	"bitbucket.org/chiffa-org/sha2n/store"
)

type Response struct {
	StatusCode int `json:"-"`

	Status string      `json:"status"`
	Data   interface{} `json:"data,omitempty"`
	Error  string      `json:"error,omitempty"`
}

func (resp *Response) Render(w http.ResponseWriter, r *http.Request) error {
	render.SetContentType(render.ContentTypeJSON)
	render.Status(r, resp.StatusCode)

	return nil
}

func NewOkResponse(code int, data interface{}) *Response {
	return &Response{
		Status:     "ok",
		StatusCode: code,
		Data:       data,
	}
}

func NewFailResponse(code int, error string) *Response {
	if error == "" {
		error = http.StatusText(code)
	}
	return &Response{
		Status:     "fail",
		StatusCode: code,
		Error:      error,
	}
}

type TaskResponse struct {
	ID         string `json:"id"`
	Payload    string `json:"payload"`
	HashRounds int    `json:"hash_rounds_cnt"`
	HashFunc   string `json:"hash_func"`
	Status     string `json:"status"`
	HashSum    string `json:"hash,omitempty"`
}

func NewTaskResponse(code int, dbTask *store.Task) *Response {
	task := &TaskResponse{
		ID:         dbTask.ID.String(),
		Payload:    dbTask.Payload,
		HashRounds: dbTask.HashRounds,
		HashFunc:   string(dbTask.HashFunc),
		Status:     "in progress",
	}
	if dbTask.HashSum != nil {
		task.Status = "finished"
		task.HashSum = hex.EncodeToString(dbTask.HashSum)
	}

	return &Response{
		Status:     "ok",
		StatusCode: code,
		Data:       task,
	}
}

type TaskIDResponse struct {
	ID string `json:"id"`
}

func NewTaskIDResponse(code int, id uuid.UUID) *Response {
	taskID := &TaskIDResponse{
		ID: id.String(),
	}

	return &Response{
		Status:     "ok",
		StatusCode: code,
		Data:       taskID,
	}
}

type TaskRequest struct {
	Payload    string `json:"payload"`
	HashRounds int    `json:"hash_rounds_cnt"`
	HashFunc   string `json:"hash_func,omitempty"`
}

func (t *TaskRequest) Bind(r *http.Request) error {
	var maxHashRounds int
	maxHashRounds, _ = r.Context().Value("max-hash-rounds").(int)

	if maxHashRounds > 0 && t.HashRounds > maxHashRounds {
		return fmt.Errorf("hash rounds counter must not exceed %d, got %d", maxHashRounds, t.HashRounds)
	}

	if t.HashRounds < 1 {
		return fmt.Errorf("hash rounds counter must be positive, got %d", t.HashRounds)
	}

	// assume SHA-256 by default
	if t.HashFunc == "" {
		t.HashFunc = "SHA-256"
	}

	if !store.HashByName(store.ParseHashName(t.HashFunc)).Available() {
		return fmt.Errorf("hash function '%s' is unavailable", t.HashFunc)
	}

	return nil
}

package api

import (
	"go.uber.org/zap"

	"bitbucket.org/chiffa-org/sha2n/processor"
	"bitbucket.org/chiffa-org/sha2n/store"
)

type API struct {
	ts   *store.Tasks
	proc *processor.Processor
	log  *zap.Logger
}

// New creates new API instance
func New(ts *store.Tasks, proc *processor.Processor, log *zap.Logger) *API {
	return &API{
		ts:   ts,
		proc: proc,
		log:  log,
	}
}

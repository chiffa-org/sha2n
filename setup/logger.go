package setup

import (
	"go.uber.org/zap"
)

// Logger bootstraps Logger instance using given config
func Logger(cfg LoggerConfig) (*zap.Logger, error) {
	zapCfg := zap.NewProductionConfig()
	if cfg.Devel {
		zapCfg = zap.NewDevelopmentConfig()
	}

	if cfg.Level != "" {
		err := zapCfg.Level.UnmarshalText([]byte(cfg.Level))
		if err != nil {
			return nil, err
		}
	}

	return zapCfg.Build()
}

package setup

import (
	"database/sql"
	"fmt"

	_ "github.com/jackc/pgx/v4/stdlib"
)

// Postgres bootstraps sql.DB instance using given config
func Postgres(cfg PostgresConfig) (*sql.DB, error) {
	db, err := sql.Open("pgx", cfg.DSN)
	if err != nil {
		return nil, fmt.Errorf("open PostgreSQL connection: %w", err)
	}

	if cfg.MaxConns > 0 {
		db.SetMaxOpenConns(cfg.MaxConns)
	}

	err = db.Ping()
	if err != nil {
		_ = db.Close()
		return nil, fmt.Errorf("ping PostgreSQL connection: %w", err)
	}

	err = InitDB(db)
	if err != nil {
		_ = db.Close()
		return nil, fmt.Errorf("init PostgreSQL structure: %w", err)
	}

	return db, nil
}

func InitDB(db *sql.DB) error {
	_, err := db.Exec(`
		CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

		CREATE TABLE IF NOT EXISTS tasks (
			task_id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
			payload text NOT NULL,
			hash_func varchar(15) NOT NULL,
			hash_rounds integer NOT NULL,
			hash_sum bytea,
			created_at timestamptz NOT NULL DEFAULT NOW(),
			updated_at timestamptz,
			deleted_at timestamptz
		);
		
		CREATE INDEX IF NOT EXISTS tasks_hash_sum_is_null_idx
			ON tasks (hash_sum) WHERE hash_sum IS NULL;
	`)

	return err
}

package setup

import (
	"context"
	"time"

	"github.com/heetch/confita"
)

type Config struct {
	Logger    LoggerConfig
	Server    ServerConfig
	Postgres  PostgresConfig
	Processor ProcessorConfig
	Input     InputConfig
}

type LoggerConfig struct {
	Devel bool   `config:"devel"`
	Level string `config:"log-level"`
}

type ServerConfig struct {
	ListenAddr      string        `config:"listen-addr"`
	GracefulTimeout time.Duration `config:"graceful-timeout"`
}

type PostgresConfig struct {
	DSN      string `config:"pg-dsn,required"`
	MaxConns int    `config:"pg-max-conns"`
	// TODO: separate r/w api/workers?
}

type ProcessorConfig struct {
	HashWorkers  int `config:"hash-workers"`
	QueueSize    int `config:"queue-size"`
	RoundsAtOnce int `config:"rounds-at-once"`
}

type InputConfig struct {
	MaxHashRounds int `config:"max-hash-rounds"`
}

func DefaultConfig() Config {
	return Config{
		Logger: LoggerConfig{
			Level: "info",
		},
		Server: ServerConfig{
			ListenAddr: ":80",
		},
		Postgres: PostgresConfig{},
		Processor: ProcessorConfig{
			HashWorkers: 1,
			QueueSize:   1,
		},
		Input: InputConfig{},
	}
}

// BootstrapConfig bootstraps Config instance from env variables
func BootstrapConfig() (Config, error) {
	cfg := DefaultConfig()

	err := confita.NewLoader().Load(context.Background(), &cfg)
	return cfg, err
}

package store

import (
	"context"
	"database/sql"

	"github.com/Masterminds/squirrel"
)

type Tasks struct {
	db squirrel.StatementBuilderType
}

// NewTasks creates new Tasks
func NewTasks(db *sql.DB) *Tasks {
	return &Tasks{
		db: squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar).RunWith(db),
	}
}

// Ping checks Tasks availability
func (ts *Tasks) Ping() error {
	return ts.PingContext(context.Background())
}

// PingContext checks Tasks availability
func (ts *Tasks) PingContext(ctx context.Context) error {
	_, err := ts.db.Select("1").ExecContext(ctx)
	return err
}

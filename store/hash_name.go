package store

import (
	"crypto"
	"strings"
)

type HashName string

const (
	SHA224     HashName = "SHA-224"
	SHA256              = "SHA-256"
	SHA384              = "SHA-384"
	SHA512              = "SHA-512"
	SHA512_224          = "SHA-512/224"
	SHA512_256          = "SHA-512/256"
)

var nameHashes = map[HashName]crypto.Hash{
	SHA224:     crypto.SHA224,
	SHA256:     crypto.SHA256,
	SHA384:     crypto.SHA384,
	SHA512:     crypto.SHA512,
	SHA512_224: crypto.SHA512_224,
	SHA512_256: crypto.SHA512_256,
}

var hashNames = map[crypto.Hash]HashName{
	crypto.SHA224:     SHA224,
	crypto.SHA256:     SHA256,
	crypto.SHA384:     SHA384,
	crypto.SHA512:     SHA512,
	crypto.SHA512_224: SHA512_224,
	crypto.SHA512_256: SHA512_256,
}

func ParseHashName(name string) HashName {
	hashName := HashName(strings.ToUpper(name))
	if _, ok := nameHashes[hashName]; ok {
		return hashName
	}
	return ""
}

func HashByName(name HashName) crypto.Hash {
	return nameHashes[name]
}

func NameByHash(hash crypto.Hash) HashName {
	return hashNames[hash]
}

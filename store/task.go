package store

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/Masterminds/squirrel"
	"github.com/gofrs/uuid"
)

type Task struct {
	ID         uuid.UUID
	Payload    string
	HashFunc   HashName
	HashRounds int
	HashSum    []byte
}

func (ts *Tasks) GetUncompletedTasks() ([]*Task, error) {
	return ts.GetUncompletedTasksContext(context.Background())
}

func (ts *Tasks) GetUncompletedTasksContext(ctx context.Context) ([]*Task, error) {
	q := ts.db.
		Select("task_id", "payload", "hash_func", "hash_rounds", "hash_sum").
		From("tasks").
		Where("hash_sum IS NULL").
		Where("deleted_at IS NULL")

	rows, err := q.QueryContext(ctx)
	if err != nil {
		return nil, fmt.Errorf("query tasks: %w", err)
	}
	defer rows.Close()

	tasks := make([]*Task, 0)

	for rows.Next() {
		task := new(Task)

		err := rows.Scan(&task.ID, &task.Payload, &task.HashFunc, &task.HashRounds, &task.HashSum)
		if err != nil {
			return nil, fmt.Errorf("fetch tasks: %w", err)
		}

		tasks = append(tasks, task)
	}

	err = rows.Err()
	if err != nil {
		return nil, fmt.Errorf("fetch tasks: %w", err)
	}

	return tasks, nil
}

func (ts *Tasks) GetTask(id uuid.UUID) (*Task, error) {
	return ts.GetTaskContext(context.Background(), id)
}

func (ts *Tasks) GetTaskContext(ctx context.Context, id uuid.UUID) (*Task, error) {
	q := ts.db.
		Select("task_id", "payload", "hash_func", "hash_rounds", "hash_sum").
		From("tasks").
		Where("task_id = ?", id).
		Where("deleted_at IS NULL")

	task := new(Task)

	err := q.ScanContext(ctx, &task.ID, &task.Payload, &task.HashFunc, &task.HashRounds, &task.HashSum)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, fmt.Errorf("query task: %w", err)
	}

	return task, nil
}

func (ts *Tasks) AddTask(payload string, hashFunc HashName, hashRounds int) (uuid.UUID, error) {
	return ts.AddTaskContext(context.Background(), payload, hashFunc, hashRounds)
}

func (ts *Tasks) AddTaskContext(ctx context.Context, payload string, hashFunc HashName, hashRounds int) (uuid.UUID, error) {
	id, err := uuid.NewV4()
	if err != nil {
		return uuid.Nil, fmt.Errorf("generate UUID: %w", err)
	}

	q := ts.db.
		Insert("tasks").
		Columns("task_id", "payload", "hash_func", "hash_rounds", "created_at").
		Values(id, payload, hashFunc, hashRounds, squirrel.Expr("NOW()"))

	qstr, _, _ := q.ToSql()
	println(qstr)

	_, err = q.ExecContext(ctx)
	if err != nil {
		return uuid.Nil, fmt.Errorf("insert task: %w", err)
	}

	return id, nil
}

func (ts *Tasks) SetTaskResult(id uuid.UUID, hashSum []byte) error {
	return ts.SetTaskResultContext(context.Background(), id, hashSum)
}

func (ts *Tasks) SetTaskResultContext(ctx context.Context, id uuid.UUID, hashSum []byte) error {
	q := ts.db.
		Update("tasks").
		Set("hash_sum", hashSum).
		Set("updated_at", squirrel.Expr("NOW()")).
		Where("task_id = ?", id)

	_, err := q.ExecContext(ctx)
	if err != nil {
		return fmt.Errorf("update task: %w", err)
	}

	return nil
}

func (ts *Tasks) DeleteTask(id uuid.UUID) error {
	return ts.DeleteTaskContext(context.Background(), id)
}

func (ts *Tasks) DeleteTaskContext(ctx context.Context, id uuid.UUID) error {
	q := ts.db.
		Update("tasks").
		Set("deleted_at", squirrel.Expr("NOW()")).
		Where("task_id = ?", id)

	_, err := q.ExecContext(ctx)
	if err != nil {
		return fmt.Errorf("delete task: %w", err)
	}

	return nil
}

package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"go.uber.org/zap"

	"bitbucket.org/chiffa-org/sha2n/api"
	"bitbucket.org/chiffa-org/sha2n/processor"
	"bitbucket.org/chiffa-org/sha2n/setup"
	"bitbucket.org/chiffa-org/sha2n/store"
)

var (
	config setup.Config
	logger *zap.Logger
)

func init() {
	var err error

	config, err = setup.BootstrapConfig()
	if err != nil {
		log.Fatalf("config bootstrap error: %v", err)
	}

	logger, err = setup.Logger(config.Logger)
	if err != nil {
		log.Fatalf("logger bootstrap error: %v", err)
	}
}

func main() {
	// Bootstrap and defer shutdown of DB connection
	db, err := setup.Postgres(config.Postgres)
	if err != nil {
		logger.Error("postgres bootstrap error", zap.Error(err))
		return
	}
	defer func() {
		err := db.Close()
		if err != nil {
			logger.Error("postgres shutdown error", zap.Error(err))
		} else {
			logger.Info("postgres shutdown completed")
		}
	}()

	ts := store.NewTasks(db)

	// Bootstrap and defer shutdown of processor
	proc, err := processor.New(
		ts, logger,
		config.Processor.HashWorkers,
		config.Processor.QueueSize,
		config.Processor.RoundsAtOnce,
	)
	if err != nil {
		logger.Error("processor bootstrap error", zap.Error(err))
		return
	}
	defer func() {
		err := proc.Close()
		if err != nil {
			logger.Error("processor shutdown error", zap.Error(err))
		} else {
			logger.Info("processor shutdown completed")
		}
	}()

	// Rerun uncompleted tasks
	tasks, err := ts.GetUncompletedTasks()
	if err != nil {
		logger.Error("get uncompleted tasks error", zap.Error(err))
		return
	}
	for _, task := range tasks {
		err = proc.AddTask(task.ID, task.Payload, store.HashByName(task.HashFunc), task.HashRounds)
		if err != nil {
			logger.Error("add task to processor", zap.Any("task id", task.ID), zap.Error(err))
		}
	}

	// Bootstrap HTTP API
	httpAPI := api.New(ts, proc, logger)

	r := chi.NewRouter()
	r.Get("/ping", httpAPI.Ping)
	r.Route("/tasks", func(r chi.Router) {
		// TODO: use zap logger for consistency
		if config.Logger.Devel {
			r.Use(middleware.Logger)
		}
		r.Use(middleware.Recoverer)

		r.With(middleware.GetHead).Get("/{task-id}", httpAPI.GetTask)
		r.With(
			middleware.AllowContentType("application/json"),
			middleware.WithValue("max-hash-rounds", config.Input.MaxHashRounds),
		).Post("/", httpAPI.PostTask)
	})

	srv := http.Server{
		Addr:    config.Server.ListenAddr,
		Handler: r,
	}

	// Define graceful shutdown
	idleConnsClosed := make(chan struct{})
	go func() {
		defer close(idleConnsClosed)

		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
		<-sigint

		logger.Info("HTTP server shutdown started", zap.Duration("timeout", config.Server.GracefulTimeout))
		var ctx context.Context
		var cancel context.CancelFunc
		if config.Server.GracefulTimeout > 0 {
			ctx, cancel = context.WithTimeout(context.Background(), config.Server.GracefulTimeout)
		} else {
			ctx, cancel = context.WithCancel(context.Background())
		}
		defer cancel()

		err := srv.Shutdown(ctx)
		if err != nil {
			// Error from closing listeners, or context timeout
			logger.Error("HTTP server Shutdown", zap.Error(err))
		}
	}()

	logger.Info("HTTP server listening started", zap.String("address", config.Server.ListenAddr))
	err = srv.ListenAndServe()
	if err != http.ErrServerClosed {
		// Error starting or closing listener
		logger.Error("HTTP server ListenAndServe", zap.Error(err))
		return
	}

	<-idleConnsClosed
	logger.Info("HTTP server shutdown completed")
}

# sha2n

sha2n [ʃɑː'tuːn] is a service that applies SHA-2 function to a given input N times.

It is supposed to run this service behind nginx or another load balancer with HTTPS termination, proper request timeouts and content length limits.

You can browse API documentation here: https://chiffa-org.bitbucket.io/sha2n/

`/ping` endpoint should not be exposed. 


# Configuration:

Service is configured completely with environment variables.


## Logging:

`LOG_LEVEL`
: log lovel (`debug`/`warn`/`info`/`error`); default: `info`

`DEVEL`
: use developer log mode: write plain logs instead of json, log http requests


## HTTP server:

`LISTEN_ADDR`
: where to listen for requests; `:80` by default

`GRACEFUL_TIMEOUT`
: time to gracefully stop all http listeners and hash workers before exit; unlimited if not set


## PostgreSQL connection:

`PG_DSN`
: connection URL string; required to start service

`PG_MAX_CONNS`
: limit max open connections to postgres; unlimited if not set


## Hash calculation:

`HASH_WORKERS`
: number of concurrent workers to porocess hash calculation tasks; 1 by default

`QUEUE_SIZE`
: set it at least to maximum amount of tasks to be in progress according to expected load and hardware recources

`ROUNDS_AT_ONCE`
: how many hash round should be processed before switching to another task


## Input validation:

`MAX_HASH_ROUNDS`
: reject tasks with hash rounds greater than this value; unlimited if not set

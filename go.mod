module bitbucket.org/chiffa-org/sha2n

go 1.13

require (
	github.com/Masterminds/squirrel v1.1.0
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/heetch/confita v0.8.0
	github.com/jackc/pgx v3.6.0+incompatible
	github.com/jackc/pgx/v4 v4.0.1
	go.uber.org/multierr v1.2.0 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
)
